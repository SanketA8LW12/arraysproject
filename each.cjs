function eachFun(arrayItems, callBack){
    
    if(Array.isArray(arrayItems)){ // if its not an array it will return -1;
        
        for(let index = 0; index < arrayItems.length; index++){
            callBack(arrayItems[index], index);
        }
    }
    else{
        console.log(-1);
    }
}

module.exports = eachFun;