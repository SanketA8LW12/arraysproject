function filteredFun(arrayItems, callBack) {

    let filteredArray = [];

    if (Array.isArray(arrayItems)) {

        for (let index = 0; index < arrayItems.length; index++) {

            let checkCallBack = callBack(arrayItems[index], index, arrayItems);

            if (checkCallBack === true) { // if the func is true we are pushing to new array 
                filteredArray.push(arrayItems[index]);
            }

        }
    }

    return filteredArray;
}

module.exports = filteredFun;
