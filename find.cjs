function findFun(arrayItems, callBack) {

    let firstMatch; // to store first value returned from callBack function

    if (Array.isArray(arrayItems)) {

        for (let index = 0; index < arrayItems.length; index++) {
            if (callBack(arrayItems[index])) {
                firstMatch = arrayItems[index];
                if (firstMatch !== undefined || firstMatch !== null) {
                    break; // if first match is found then break
                }
            }
        }
    }

    return firstMatch;
}

module.exports = findFun;
