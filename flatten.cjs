//let a = [1,2,[8,10], 3, [4,5,6], [1,[2,[6,[100,[300,45,56,[200]]]]]]];

function flatFun(array, depth) {

    let ans = []; // array to store all element after flaten

    if (depth === undefined || depth === null) {
       // console.log("entering if of ")
        depth = 1;
    }

    function arrFlat(valueArray, depth) {
       // console.log(depth + " depth")
         
        for (let index = 0; index < valueArray.length; index++) {
           
            if (index in valueArray) {
                if (Array.isArray(valueArray[index]) && depth > 0) {
                    // recursively calling arrFlat()function if array is present at particular index 
                    arrFlat(valueArray[index], depth - 1); 
                }
                else {
                    ans.push(valueArray[index]);
                }
            }

        }    

    }
    console.log(depth + " the depgth before fun call")
    arrFlat(array,depth);
    return ans;
}

//flatFun(a);

module.exports = flatFun;
