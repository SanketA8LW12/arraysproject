function mapFun(arrayItems, callBack) {

    let mapArray = []; // new array to store value returned from callBack function

    if (Array.isArray(arrayItems)) {

        for (let index = 0; index < arrayItems.length; index++) {
            mapArray.push(callBack(arrayItems[index], index, arrayItems));
        }
    }

    return mapArray; // returning new mapped array
}

module.exports = mapFun;