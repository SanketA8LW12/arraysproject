function reduceFun(arrayItems, callBack, startValue) {

    let reduceResult = startValue;

    if (Array.isArray(arrayItems)) {

        if (reduceResult === undefined) { // if starting value is not passed

            if(arrayItems.length === 0){
                return 0;
            }

            reduceResult = arrayItems[0]; // assigning value from array and starting for loop from index 1
            
            for (let index = 1; index < arrayItems.length; index++) {
                //reduceResult = callBack(arrayItems[index], index, reduceResult, arrayItems);
                reduceResult = callBack(reduceResult, arrayItems[index], index, arrayItems);
            }
        }
        else {

            for (let index = 0; index < arrayItems.length; index++) {
                //reduceResult = callBack(arrayItems[index], index, reduceResult, arrayItems);
                reduceResult = callBack(reduceResult, arrayItems[index], index, arrayItems);
            }

        }
    }
   
    return reduceResult;
}

module.exports = reduceFun;
